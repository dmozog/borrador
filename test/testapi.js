//
// Se declaran los paquetes que se van a utilizar en el JavaScript
//
var mocha = require('mocha');
var chai = require('chai');
var chaihttp = require('chai-http');


//
// Se indica que se va a hacer uso de chaihttp en chai
//
chai.use(chaihttp);


//
// Se declara la variable should (resultado esperado)
//
var should = chai.should();


//
// Se declara la variable server para que arranque el server
// De ese modo el test es autocontenido, no hay que arrancar el servidor
// por fuera
//
var server = require('../server')


//
// Se decriben los test. Los test se agrupan por suites
// Este test solo prueba que se llega a una web http
// Cada bloque IT ejecuta un test unitario
//
describe('First test suite',
  function() {
    it('[LOG] Test that DuckDuckGo works',
      function(done) {
        chai.request('http://www.duckduckgo.com')
          .get('/')
          .end(
            function(err, res){
              console.log("[LOG] Request has ended");
              // console.log(err);
              // console.log(res);
              //
              // La respuesta correcta debe ser http:200. Para que consideremos
              // el test como correcto consideramos que la respuesta debe ser
              // http:200
              //
              res.should.have.status(200);
              done();
            }
          )
      }
    )
  }
);


//
// Este test prueba nuestra aplicación
// Cada bloque IT ejecuta un test unitario
//
describe('Test de API de Usuarios de TechU',
  function() {
    it('[LOG] Prueba de que la API de usuarios funciona OK',
      function(done) {
        chai.request('http://localhost:3000')
          .get('/apitechu/v1')
          .end(
            function(err, res){
              console.log("[LOG] Request has ended");
              // console.log(err);
              //
              // Se ejecutan dos pruebas:
              //   - La respuesta correcta debe ser http:200
              //   - Se comprueba la respuesta del metodo GET
              //
              res.should.have.status(200);
              res.body.msg.should.be.eql("Respuesta JSON desde API TechU");
              done();
            }
          )
      }
    ),
    it('[LOG] Prueba de que la API devuelve una lista de usuarios correctos',
      function(done) {
        chai.request('http://localhost:3000')
          .get('/apitechu/v1/users')
          .end(
            function(err, res){
              console.log("[LOG] Request has ended");
              //
              // Se ejecutan dos pruebas:
              //   - La respuesta correcta debe ser http:200
              //   - Se comprueba la respuesta del metodo GET:
              //     - Que es de tipo array
              //     - Que la lista es del tipo esperado
              //
              res.should.have.status(200);
              res.body.should.be.a("array");
              for (user of res.body) {
                user.should.have.property('email');
                user.should.have.property('password');
              }
              done();
            }
          )
      }
    )
  }
);
