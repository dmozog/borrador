###################################################################
# mozo 09/05/2018:
#
# Dockerfile: Fichero de definición del docker
###################################################################

#
# Se indica el origen
FROM node

#
# Se indica el directorio de trabajo
WORKDIR /apitechu

#
# Se incorpora todo el contenido del directorio al directorio de trabajo
ADD . /apitechu

#
# Se indica el puerto de escucha
EXPOSE 3000

#
# Se indica el comando a arrancar
CMD ["npm", "start"]
