////////////////////////////////////////////////////////////////////////////////
// --- dakar ---
// server.js
//
// Fichero sever de la aplicación dakar
// Este fichero contine la información de los métodos necesarios para el
// backend
////////////////////////////////////////////////////////////////////////////////
//
//
console.log ("------- --------------- -------");
console.log ("------------ dakar ------------");
console.log ("------ banking made easy ------");
console.log ("------ ----------------- ------");
var express =require('express') ;
var bcrypt = require('bcryptjs');
var app = express();
var sizeSaltSync =10;
var port = process.env.PORT || 3000;
var bodyParser = require ('body-parser');
app.use(bodyParser.json());
var baseMLabURL ="https://api.mlab.com/api/1/databases/apitechugargallo/collections/";
var mLabAPIKey = "apiKey=FceI0W1GziU2Z4HoA0w5Io79gO95P_IL";
var requestJson = require('request-json');
app.listen(port);
console.log("-------------------------------------------");
console.log("API dakar escuchando en el puerto: " + port);
console.log("-------------------------------------------");


//
// gargallo&mozo 22/05/2018:
//
app.get("/apitechu/v1",
  function(req,res) {
    console.log("GET /apitechu/v1");
    //res.send("Respuesta desde API TechU");
    res.send ({"msg" : "Hola desde APITech"});
  }
);


//
// gargallo&mozo 22/05/2018:
//
  app.get("/apitechu/v1/users",
    function(req,res) {
      console.log("GET /apitechu/v1/users");
      res.sendFile('usuarios.json',{root: __dirname});
      /*var users = require ('./usuarios.json');
      res.send(users);*/
    }
  );


//
// gargallo&mozo 22/05/2018:
//
  app.get("/apitechu/v2/users",
    function(req,res) {
      console.log("GET /apitechu/v2/users");
      var httpClient = requestJson.createClient(baseMLabURL);
      console.log("Cliente HTTP creado");
      httpClient.get ("user?" + mLabAPIKey,
        function(err,resMLab,body){
          var response = !err ? body : {"msg" : "Error obteniendo usuarios"}
          res.send(response);
        }
      )
    }
  );


//
// gargallo&mozo 22/05/2018:
//
  app.get("/apitechu/v2/users/:id",
    function(req,res) {
      console.log("GET /apitechu/v2/users/:id");
      var id = req.params.id;
      console.log("GET /apitechu/v2/users/:id-" + id+"-");
      var query = 'q={"id":' + id + '}';
      var httpClient = requestJson.createClient(baseMLabURL);
      console.log("Cliente HTTP creado");
      httpClient.get ("user?" + query  + "&" + mLabAPIKey,
        function(err,resMLab,body){
          //var response = !err ? body : {"msg" : "Error obteniendo usuario " + id}
          var response = {};
          if (err){
            response =  {"msg" : "Error obteniendo usuario " + id};
            res.status(500);
          } else {
            if (body.length > 0)
            {
              response = body;
            } else {
              response = {"msg": "Usuario no encontrado"};
              res.status (404);
            }
          }
          res.send(response);
        }
      )
    }
  );

//
// gargallo&mozo 22/05/2018:
//
  app.post("/apitechu/v1/users",
    function (req,res){
      console.log ("POST  /apitechu/v1/users");
      console.log("first_name is " + req.body.first_name);
      console.log("last_name is " + req.body.last_name);
      console.log("country is " + req.body.country);
      var newUser ={
        "first_name": req.body.first_name,
        "last_name" : req.body.last_name,
        "country" : req.body.country
      };
      var users = require ('./usuarios.json');
      //users.push(req.body);
      users.push(newUser);
      writeUserDataToFile(users);

      console.log("Usuario guardado con exito ");
      res.send({"msg" : "Usuario guardado con exito"});

    }
);


//
// gargallo&mozo 22/05/2018:
//
    app.delete("/apitechu/v1/users/:id",
      function (req,res){
        console.log ("DELETE  /apitechu/v1/users");
        console.log (req.params);
        console.log (req.params.id);
        var users = require ('./usuarios.json');
        users.splice(req.params.id-1,1);
        writeUserDataToFile(users);
        console.log ("usuario borrado");
          res.send({"msg" : "Usuario borrado con exito"});
    }

  );


//
// gargallo&mozo 22/05/2018:
//
function writeUserDataToFile(data) {
  var fs = require ('fs');
  var jsonUserData = JSON.stringify(data,undefined,2);
  fs.writeFile("./login_usuarios.json",jsonUserData,"utf8",
  function (err){
    if (err) {
        console.log(err);
    } else {
        console.log("Datos escritos en archivo");
    }
  }
);
}

//
// gargallo&mozo 22/05/2018:
//
app.post("/apitechu/v1/monstruo/:p1/:p2",
  function (req,res){
    console.log("Parametros ");
    console.log(req.params);
    console.log("Query String ");
    console.log(req.query);
    console.log("Body ");
    console.log(req.body);
    console.log("Headers ");
    console.log(req.headers);
  }
);


//
// gargallo&mozo 22/05/2018:
//
app.post("/apitechu/v1/login",
  function (req,res){
    console.log ("POST  /apitechu/v1/login");
    console.log("first_name is " + req.body.email);
    console.log("last_name is " + req.body.password);
    var newUser ={
      "email": req.body.email,
      "password" : req.body.password,
    };
    var users = require ('./login_usuarios.json');
    var salida={"mensaje" : "Login incorrecto" };
    for (user of users) {
      if  (user.email == newUser.email)
      {
        console.log("Usuario encontrado "  + user.id);
        if (user.password == newUser.password)
        {
          console.log("Password correcta");
          user.logged= true;
          //users.push(user);
          writeUserDataToFile(users);
          salida={"mensaje" : "Login correcto", "idUsuario" : user.id };
          break;
        }
        else{
          console.log("Password incorrecta");
          break;
        }

      }
      else {
          console.log("Usuario no encontrado "  + user.id);
      }

    }
    //users.push(req.body);


    res.send(salida);

  }
);


//
// gargallo&mozo 22/05/2018:
//
//
//
app.post("/apitechu/v2/login",
  function (req,res){
    console.log ("POST  /apitechu/v2/login");
    console.log("email is " + req.body.email);
    console.log("password is " + req.body.password);
    //var query = 'q={"email":"' + req.body.email + '","password":"'+ req.body.password + '"}';
    var query = 'q={"email":"' + req.body.email + '"}';
    console.log("Login_Lanzo peticion " + "user?" + query  + "&" + mLabAPIKey  );
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Login_Cliente HTTP creado");
    httpClient.get ("user?" + query  + "&" + mLabAPIKey,
      function(err,resMLab,body){
        var response = {};
        if (err){
          response =  {"msg" : "Login_Error obteniendo usuario " + req.body.email};
          res.status(500);
          res.send(response);
        } else {
          if (body.length > 0)
          {
            var salt = bcrypt.genSaltSync(sizeSaltSync);
            var idUser= body[0].id;
            var passwd= body[0].password;
            console.log(passwd);
            console.log(req.body.password);
            if (bcrypt.compareSync(req.body.password,passwd) )
            //if (true)
            {
              var putBody = '{"$set":{"logged":true}}';
              httpClient.put ("user?" + query  + "&" + mLabAPIKey,JSON.parse(putBody),
              function(err2,resMLab2,body){
                if (err){
                  response =  {"msg" : "Login_Error al hacer el put" };
                  res.status(500);
                  res.send(response);
                } else {
                    response =  {"msg" : "Login_Usuario logado correctamente, OK al hacer el put","id":idUser};
                    console.log("Login_Ha hecho el PUT");
                }
                res.send(response);
              });
            }else{
              response = {"msg": "Login_Usuario password erronea"};
              res.status (404);
              res.send(response);
            }

          } else {
            response = {"msg": "Login_Usuario no encontrado"};
            res.status (404);
            res.send(response);
          }
        }
      });
});


//
// gargallo&mozo 22/05/2018:
//
app.post("/apitechu/v0/logout",
  function (req,res){
    console.log ("POST  /apitechu/v2/logout");
    console.log("email is " + req.body.email);
    var query = 'q={"email":"' + req.body.email + '","logged":true}';
    console.log("Logout_Lanzo peticion " + "user?" + query  + "&" + mLabAPIKey  );
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Logout_Cliente HTTP creado");
    httpClient.get ("user?" + query  + "&" + mLabAPIKey,
      function(err,resMLab,body){
        var response = {};
        if (err){
          response =  {"msg" : "Logout_Error obteniendo usuario " + req.body.email};
          res.status(500);
          res.send(response);
        } else {
          if (body.length > 0)
          {
            var putBody = '{"$unset":{"logged":""}}';
            httpClient.put ("user?" + query  + "&" + mLabAPIKey,JSON.parse(putBody),
            function(err2,resMLab2,body){
              if (err){
                response =  {"msg" : "Logout_Error al hacer el put" };
                res.status(500);
                res.send(response);
              } else {
                  response =  {"msg" : "Logout_Usuario deslogado correctamente, OK al hacer el put"};
                  console.log("Logout_Ha hecho el PUT");
              }
              res.send(response);
            });
          } else {
            response = {"msg": "Logout_Usuario no encontrado"};
            res.status (404);
            res.send(response);
          }
        }
      });
});


//
// gargallo&mozo 22/05/2018:
//
app.post("/apitechu/v1/logout",
  function (req,res){
    console.log ("POST  /apitechu/v1/logut");
    console.log("id is " + req.body.id);
    var id =req.body.id;
    var users = require ('./login_usuarios.json');
    var salida={"mensaje" : "Logout incorrecto" };
    for (user of users) {
      if  (user.id == id)
      {
        console.log("Usuario encontrado "  + user.id);
        if (user.logged)
        {
          console.log("Usuario estaba logado");
          delete user.logged;
          //users.push(user);
          writeUserDataToFile(users);
          salida={"mensaje" : "Logout correcto", "idUsuario" : user.id };
          console.log("Usuario deslogado");
          break;
        }
        else{
          console.log("Usuario no estaba logado");
          break;
        }

      }
      else {
          console.log("Usuario no encontrado "  + user.id);
      }

    }
    //users.push(req.body);


    res.send(salida);

  }
);

//
// gargallo&mozo 22/05/2018:
//
app.post("/apitechu/v2/changePasswd",
  function (req,res){
    var response = {};
    console.log ("POST  /apitechu/v2/changePasswd");
    console.log("email is " + req.body.email);
    console.log("password is " + req.body.password);
    console.log("password1 is " + req.body.password1);
    console.log("password2 is " + req.body.password2);
    if (req.body.password1!=req.body.password2)
    {
      response =  {"msg" : "changePass_Error contraseñas no son iguales"};
      console.log("changePass_Error contraseñas no son iguales");
      res.status(500);
      res.send(response);
    }
    else {
          //var query = 'q={"email":"' + req.body.email + '","password":"'+ req.body.password + '"}';
          var query = 'q={"email":"' + req.body.email +'"}';
          console.log("changePass_peticion " + "user?" + query  + "&" + mLabAPIKey  );
          var httpClient = requestJson.createClient(baseMLabURL);
          console.log("changePass__Cliente HTTP creado");
          httpClient.get ("user?" + query  + "&" + mLabAPIKey,
          function(err,resMLab,body){
          if (err){
            response =  {"msg" : "changePass__Error obteniendo usuario " + req.body.email,
                          "reserror" : true };
            console.log("changePass__Error obteniendo usuario " + req.body.email);
            res.status(500);
            res.send(response);
          } else {
            if (body.length > 0)
            {
              var salt = bcrypt.genSaltSync(sizeSaltSync);
              var idUser= body[0].id;
              var passwd= body[0].password;
              console.log(passwd);
              console.log(req.body.password);

              if (bcrypt.compareSync(req.body.password,passwd) )
              //if (true)
              {
                console.log("id es " + idUser);
                var passEncrypted = bcrypt.hashSync(req.body.password1, salt);
                var putBody = '{"$set":{"password":"'+ passEncrypted + '"}}';
                //var putBody = '{"$set":{"email":"cgargallo@gmail.com"}}';
                httpClient.put ("user?" + query  + "&" + mLabAPIKey,JSON.parse(putBody),
                function(err2,resMLab2,body){
                  if (err){
                      response =  {"msg" : "changePass_Error al hacer el put" ,
                              "reserror" : true };
                      console.log("changePass__Error al hacer el put");
                      res.status(500);
                      res.send(response);
                    } else {
                      response =  {"msg" : "changePass_Usuario cambio passwod OK correctamente, OK al hacer el put","id": idUser,
                                "reserror" : false };
                    console.log("changePass_hecho el PUT");
                  }
                  res.send(response);
                });
              }else  {
                response = {"msg": "changePass_Usuario password erronea",
                              "reserror" : true};
                  console.log("changePass_ password erronea");
                res.status (404);
                res.send(response);
              }
              } else {
            response = {"msg": "changePass_Usuario no encontrado",
                          "reserror" : true};
              console.log("changePass_Usuario no encontrado");
            res.status (404);
            res.send(response);
          }
        }
      });
    }
});


//
// gargallo&mozo 22/05/2018:
//
app.post("/apitechu/v2/addUser",
  function (req,res){
    var response = {};
    console.log ("POST  /apitechu/v2/addUser");
    console.log("nif is " + req.body.nif);
    console.log("first_name is " + req.body.first_name);
    console.log("last_name is " + req.body.last_name);
    console.log("email is " + req.body.email);
    console.log("password is " + req.body.password);
    console.log("password1 is " + req.body.password1);

    if (req.body.password!=req.body.password1)
    {
      response =  {"msg" : "addUser_Error contraseñas no son iguales"};
      console.log("addUser_Error contraseñas no son iguales");
      res.status(500);
      res.send(response);
    }
    else {
          var query = 'q={"email":"' + req.body.email + '"}';
          console.log("addUser_peticion " + "user?" + query  + "&" + mLabAPIKey  );
          var httpClient = requestJson.createClient(baseMLabURL);
          console.log("addUser_Cliente HTTP creado");
          httpClient.get ("user?" + query  + "&" + mLabAPIKey,
          function(err,resMLab,body){
          if (err){
            response =  {"msg" : "addUser_Error obteniendo usuario " + req.body.email};
            console.log("addUser__Error obteniendo usuario " + req.body.email);
            res.status(500);
            res.send(response);
          } else {
            if (body.length > 0)
            {
              response =  {"msg" : "addUser El usuario ya existe"};
              console.log("addUser El usuario ya existe");
              res.status (404);
              res.send(response);

          } else {
            //sacamos el max id
            var query = 'f={"id":1}&s={"id":-1}';
            //var query = '&s={"id":-1}';

            console.log("addUser_peticion sacar max id" + "user?" + query  + "&" + mLabAPIKey  );
            var httpClient = requestJson.createClient(baseMLabURL);
            console.log("addUser_Cliente HTTP creado");
            httpClient.get ("user?" + query  + "&" + mLabAPIKey,
            function(err,resMaxLab,bodyM){
            if (err){
              response =  {"msg" : "addUser_Error obteniendo usuario max "};
              console.log("addUser__Error obteniendo usuario max ");
              res.status(500);
              res.send(response);
            } else {
              console.log("Por aqui");
              if (bodyM.length > 0)
              {
                var maxId = bodyM[0].id;
                console.log("addUser El mas usuer es " + maxId);
                console.log("addUser_Usuario no encontrado. continuamos");
                //console.log(" newUser = '{first_name:'" + req.body.first_name + ',last_name is:' + req.body.last_name + ',email:' + req.body.email + ',password:' + req.body.password + '}"';
                var salt = bcrypt.genSaltSync(sizeSaltSync);
                var passEncrypted = bcrypt.hashSync(req.body.password1, salt);
                var newUser = {
                  "id":  maxId+1,
                  "nif":  req.body.nif,
                  "first_name":  req.body.first_name,
                  "last_name": req.body.last_name,
                  "email": req.body.email ,
                  "password": passEncrypted
                }
                console.log("new user is " + newUser);
                httpClient.post("user?" + mLabAPIKey ,newUser,
                function(err,resMLab,body){
                if (err){
                  response =  {"msg" : "addUser_Error al insertar usuario"};
                  console.log("addUser__Error al insertar usuario");
                  res.status(500);
                  res.send(response);
                } else {
                  response =  {"msg" : "addUser_Usuario dado del alta"};
                  console.log("addUser_Usuario dado del alta");
                  res.send(response);

                }
              }

            ) // httpClient
          } //  if (body.length > 0)
        } //else {
      } //functoin
    ) //httpClient
  }  //else de sacamos el max
} //else de iinea 395
}//funcin
    );
  } //else 383
}
);


//
// gargallo&mozo 22/05/2018:
//
app.get("/apitechu/v2/users/:id/accounts",
  function(req,res) {
    console.log("GET /apitechu/v2/users/:id/accounts");
    var id = req.params.id;
    var query = 'q={"user_id":' + id + '}';
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente HTTP creado");
    httpClient.get ("account?" + query  + "&" + mLabAPIKey,
      function(err,resMLab,body){
        var response = {};
        if (err){
          response =  {"msg" : "Error obteniendo cuentas usuario " + id};
          res.status(500);
        } else {
          if (body.length > 0)
          {
            response = body;
          } else {
            response = {"msg": "Cuentas no encontradas"};
            res.status (404);
          }
        }
        res.send(response);
      }
    )
  }
);


//
// gargallo&mozo 22/05/2018:
//
// Se define el método GET  para recuperar el listado de movimientos de una cuenta
// Se devuelve una respuesta JSON
// Se devuelve un listado de movimientos
//
app.get("/apitechu/v2/accounts/:iban/",
  function(req,res) {
    console.log("GET /apitechu/v2/accounts/:iban");
    var iban = req.params.iban;
    var query = 'q={"iban":"' + iban + '"}&s={"date":-1}';
    var httpClient = requestJson.createClient(baseMLabURL);
    //console.log("Cliente HTTP creado: movement?" + query  + "&" + mLabAPIKey);
    console.log("Cliente HTTP creado");
    httpClient.get ("movement?" + query  + "&" + mLabAPIKey,
      function(err,resMLab,body){
        var response = {};
        if (err){
          response =  {"msg" : "Error obteniendo movimientos cuenta " + iban};
          res.status(500);
        } else {
          if (body.length > 0)
          {
            response = body;

          } else {
            response = {"msg": "No hat movimientos para iban: " + iban};
            res.status (404);
          }
        }
        res.send(response);
      }
    )
  }
);


//
// gargallo&mozo 22/05/2018:
//
// Se define el método POST para logout
// Se devuelve una respuesta JSON
// Se devuelve resultado ok o no ok y se desloga al usuario
//
app.post("/apitechu/v2/logout",
  function(req, res) {
    console.log("POST /apitechu/v2/logout");
    console.log("id is " + req.body.id);

    // Se crea el cliente http con la URL de base que se ha definido
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("HTTP client created")

    // Se comprueba el usuario id
    // Se crea la URL de consulta de la coleccion USER
    // Se responde
    var idUser = req.body.id;
    var query = 'q={"id":' + idUser + '}';
    var query2 = "user?" + query + "&" + mLabAPIKey;
    console.log(query);
    console.log(query2);

    // Se lanza la consulta construida en la URL
    httpClient.get("user?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body){
        var response = {};
        var userLogged = false;

        if (err) {
          response = {
            "msg" : "[500] Error logouting user",
            "reserror" : true
          }
          res.status(500);
          res.send(response);
        } else {
            if (body.length > 0) {
              response = body;
              userLogged = true;

              // Usuario existe, se marca como  no logado
              var putBody = '{"$unset" : {"logged" : ""}}';
              httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                function(errPut, resMLabPut, bodyPut) {
                  var responsePut = {};
                  if (errPut)
                  {
                    response = {
                      "msg" : "[500] Error logouting user (PUT)",
                      "reserror" : true
                    }
                    res.status(500);
                    res.send(response);
                  }
                  else
                  {
                    response = {
                      "msg" : "Logout correct for user",
                      "reserror" : false
                    };
                    res.send(response);
                  }
                } // function
              );
            }
            else {
              // Usuario no existe, se devuelve error
              response = {
                "msg" : "[404] User not found",
                "reserror" : true
              }
              res.status(404);
              res.send(response);
            }
        }
      }
    );
  }
);


//
// gargallo&mozo 22/05/2018:
//
// Se define el método POST para la modificación de usuario
// Se devuelve una respuesta JSON
// Se devuelve resultado ok o no ok
//
app.post("/apitechu/v2/modifyUser",
  function(req, res) {
    console.log("POST /apitechu/v2/modifyUser");
    console.log("id is " + req.body.id);
    console.log("new_first_name is " + req.body.first_name);
    console.log("new_last_name is " + req.body.last_name);
    console.log("new_email is " + req.body.email);

    // Se crea el cliente http con la URL de base que se ha definido
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("HTTP client created")

    // Se comprueba el usuario id
    // Se crea la URL de consulta de la coleccion user
    // Se responde
    var idUser = req.body.id;
    var query = 'q={"id":' + idUser + '}';
    console.log(query);

    // Se lanza la consulta construida en la URL
    httpClient.get("user?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body){
        var response = {};

        if (err) {
          response = {
            "msg" : "[modifyUser: 500] Error checking user exists",
            "reserror" : true
          }
          res.status(500);
          res.send(response);
        } else {
            if (body.length > 0) {
              response = body;

              // Usuario existe, se modifican los campos del body
              var putBody = '{"$set":{"first_name":"'+ req.body.first_name + '"'
                  +',"last_name":"'+ req.body.last_name + '"'
                  +',"email":"'+ req.body.email + '"}}';

              httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                function(errPut, resMLabPut, bodyPut) {
                  var responsePut = {};
                  if (errPut)
                  {
                    response = {
                      "msg" : "[modifyUser: 500] Error setting values",
                      "reserror" : true
                    }
                    res.status(500);
                    res.send(response);
                  }
                  else
                  {
                    response = {
                      "msg" : "[modifyUser] User correctly modified",
                      "reserror" : false
                    };
                    res.send(response);
                  }
                } // function put
              );
            }
            else {
              // Usuario no existe, se devuelve error
              response = {
                "msg" : "[modifyUser: 404] User not found",
                "reserror" : true
              }
              res.status(404);
              res.send(response);
            }
        }
      }
    );
  } // function post manejadora
); // post modifyUser


//
// gargallo&mozo 22/05/2018:
//
// Se define el método POST para la transferencia
// Se asume que el procesamiento de la transferencia en la cuenta destino no
//   se realiza: no habrá modificación del saldo en cuenta destino
// Se devuelve una respuesta JSON
// Se devuelve resultado ok o no ok
//
app.post("/apitechu/v2/makeTransfer",
  function(req, res) {
    console.log("[makeTransfer] POST /apitechu/v2/makeTransfer");

    // Se crea el cliente http con la URL de base que se ha definido
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("[makeTransfer] HTTP client created")

    // Se comprueba la existencia de la cuenta origen
    var iban = req.body.iban;
    var query = 'q={"iban":"' + iban + '"}';
    console.log(query);

    // Se lanza la consulta construida en la URL
    httpClient.get("account?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body){
        var response = {};

        if (err) {
          // Error en la consulta de la cuenta
          response = {
            "msg" : "[makeTransfer: 500] Error checking account",
            "reserror" : true
          }
          res.status(500);
          res.send(response);

        } else {

            if (body.length > 0) {
              // La cuenta existe, se chequea el saldo
              // En body[0].balance se dispone del saldo
              // En req.body.amount se dispone del importe
              var balance = body[0].balance;
              var amount = req.body.amount;
              console.log("[makeTransfer] Balance: " + balance);
              console.log("[makeTransfer] Amount: " + amount);

              if (balance >= amount)
              {
                // Saldo suficiente: se actualiza saldo y se inserta movimiento
                var newBalance = balance - amount;
                var putBody = '{"$set":{"balance":'+  newBalance + '}}';
                console.log(putBody);
                httpClient.put("account?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                  function(errPut, resMLabPut, bodyPut) {
                    var responsePut = {};
                    if (errPut)
                    {
                      // error en Put: no se actualiza saldo ni se inserta movimiento
                      response = {
                        "msg" : "[makeTransfer: 500] Error updating balance",
                        "reserror" : true
                      }
                      res.status(500);
                      res.send(response);
                    }
                    else
                    {
                      // Saldo actualizado. Se inserta movimiento
                      var fecha = new Date();
                      var newMovement = {
                        "iban": req.body.iban,
                        "other_iban":  req.body.other_iban,
                        "other_name":  req.body.other_name,
                        "type": "cargo",
                        "sub_type": "Transferencia" ,
                        "concept": req.body.concept,
                        "currency": req.body.currency,
                        "date": fecha,
                        "amount": amount
                      };

                      httpClient.post("movement?" + mLabAPIKey ,newMovement,
                      function(err,resMLabMov,bodyPost){
                      if (err){
                        // error en la insercion de movimiento
                        response = {
                          "msg" : "[makeTransfer: 500] Error inserting movement",
                          "reserror" : true
                        }
                        res.status(500);
                        res.send(response);
                      } else
                      {
                        // Movimiento insertado correctamente
                        response = {
                          "msg" : "[makeTransfer] Transfer correctly made",
                          "reserror" : false
                        };
                        res.send(response);                      }
                    }
                  ); // httpClient
                    }
                  } // function put
                );
              }
              else {
                // Saldo insuficiente: la transferencia no se realiza
                response = {
                    "msg" : "[makeTransfer] Transfer not made: not enough balance",
                    "reserror" : true
                  }
                  res.send(response);
              }
            }
            else {
              // La cuenta no existe, se devuelve error
              response = {
                "msg" : "[makeTransfer: 404] Account not found",
                "reserror" : true
              }
              res.status(404);
              res.send(response);
            }
        }
      }
    );
  } // function post makeTransfer
); // post makeTransfer


//
// gargallo&mozo 22/05/2018:
//
// Se define el método POST para el ingreso
// Se devuelve una respuesta JSON
// Se devuelve resultado ok o no ok
//
app.post("/apitechu/v2/makeIncome",
  function(req, res) {
    console.log("[makeIncome] POST /apitechu/v2/makeIncome");

    // Se crea el cliente http con la URL de base que se ha definido
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("[makeIncome] HTTP client created")

    // Se comprueba la existencia de la cuenta origen
    var iban = req.body.iban;
    var query = 'q={"iban":"' + iban + '"}';
    console.log(query);

    // Se lanza la consulta construida en la URL
    httpClient.get("account?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body){
        var response = {};

        if (err) {
          // Error en la consulta de la cuenta
          response = {
            "msg" : "[makeIncome: 500] Error checking account",
            "reserror" : true
          }
          res.status(500);
          res.send(response);

        } else {

            if (body.length > 0) {
              // La cuenta existe, se chequea el saldo
              // En body[0].balance se dispone del saldo
              // En req.body.amount se dispone del importe
              var balance = body[0].balance;
              var amount = req.body.amount;
              console.log("[makeIncome] Balance: " + balance);
              console.log("[makeIncome] Amount: " + amount);

                // Saldo suficiente: se actualiza saldo y se inserta movimiento
                var newBalance = balance + amount;
                var putBody = '{"$set":{"balance":'+  newBalance + '}}';
                console.log(putBody);
                httpClient.put("account?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                  function(errPut, resMLabPut, bodyPut) {
                    var responsePut = {};
                    if (errPut)
                    {
                      // error en Put: no se actualiza saldo ni se inserta movimiento
                      response = {
                        "msg" : "[makeIncome: 500] Error updating balance",
                        "reserror" : true
                      }
                      res.status(500);
                      res.send(response);
                    }
                    else
                    {
                      // Saldo actualizado. Se inserta movimiento
                      var fecha = new Date();
                      var newMovement = {
                        "iban": req.body.iban,
                        "type": "abono",
                        "sub_type": "Ingreso" ,
                        "concept": req.body.concept,
                        "currency": req.body.currency,
                        "date": fecha,
                        "amount": amount
                      };

                      httpClient.post("movement?" + mLabAPIKey ,newMovement,
                      function(err,resMLabMov,bodyPost){
                      if (err){
                        // error en la insercion de movimiento
                        response = {
                          "msg" : "[makeIncome: 500] Error inserting movement",
                          "reserror" : true
                        }
                        res.status(500);
                        res.send(response);
                      } else
                      {
                        // Movimiento insertado correctamente
                        response = {
                          "msg" : "[makeIncome] Income correctly made",
                          "reserror" : false
                        };
                        res.send(response);                      }
                    }
                  ); // httpClient
                    }
                  } // function put
                );
            }
            else {
              // La cuenta no existe, se devuelve error
              response = {
                "msg" : "[makeIncome: 404] Account not found",
                "reserror" : true
              }
              res.status(404);
              res.send(response);
            }
        }
      }
    );
  } // function post makeIncome
); // post makeIncome


//
// gargallo&mozo 28/05/2018:
//
// Se define el método POST para el reintegro
// Se devuelve una respuesta JSON
// Se devuelve resultado ok o no ok
//
app.post("/apitechu/v2/makeRefund",
  function(req, res) {
    console.log("[makeRefund] POST /apitechu/v2/makeRefund");

    // Se crea el cliente http con la URL de base que se ha definido
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("[makeRefund] HTTP client created")

    // Se comprueba la existencia de la cuenta origen
    var iban = req.body.iban;
    var query = 'q={"iban":"' + iban + '"}';
    console.log(query);

    // Se lanza la consulta construida en la URL
    httpClient.get("account?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body){
        var response = {};

        if (err) {
          // Error en la consulta de la cuenta
          response = {
            "msg" : "[makeRefund: 500] Error checking account",
            "reserror" : true
          }
          res.status(500);
          res.send(response);

        } else {

            if (body.length > 0) {
              // La cuenta existe, se chequea el saldo
              // En body[0].balance se dispone del saldo
              // En req.body.amount se dispone del importe
              var balance = body[0].balance;
              var amount = req.body.amount;
              console.log("[makeRefund] Balance: " + balance);
              console.log("[makeRefund] Amount: " + amount);
              if (balance >= amount)
              {
                // Saldo suficiente: se actualiza saldo y se inserta movimiento
                var newBalance = balance - amount;
                var putBody = '{"$set":{"balance":'+  newBalance + '}}';
                console.log(putBody);
                httpClient.put("account?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                  function(errPut, resMLabPut, bodyPut) {
                    var responsePut = {};
                    if (errPut)
                    {
                      // error en Put: no se actualiza saldo ni se inserta movimiento
                      response = {
                        "msg" : "[makeRefund: 500] Error updating balance",
                        "reserror" : true
                      }
                      res.status(500);
                      res.send(response);
                    }
                    else
                    {
                      // Saldo actualizado. Se inserta movimiento
                      var fecha = new Date();
                      var newMovement = {
                        "iban": req.body.iban,
                        "type": "cargo",
                        "sub_type": "Reintegro" ,
                        "concept": req.body.concept,
                        "currency": req.body.currency,
                        "date": fecha,
                        "amount": amount
                      };

                      httpClient.post("movement?" + mLabAPIKey ,newMovement,
                      function(err,resMLabMov,bodyPost){
                      if (err){
                        // error en la insercion de movimiento
                        response = {
                          "msg" : "[makeRefund: 500] Error inserting movement",
                          "reserror" : true
                        }
                        res.status(500);
                        res.send(response);
                      } else
                      {
                        // Movimiento insertado correctamente
                        response = {
                          "msg" : "[makeRefund] Refund correctly made",
                          "reserror" : false
                        };
                        res.send(response);                      }
                    }
                  ); // httpClient
                    }
                  } // function put
                );
              }
              else {
                // Saldo insuficiente
                response = {
                  "msg" : "[makeRefund] No enough balance",
                  "reserror" : true
                }
                res.status(404);
                res.send(response);
              }
            }
            else {
              // La cuenta no existe, se devuelve error
              response = {
                "msg" : "[makeRefund: 404] Account not found",
                "reserror" : true
              }
              res.status(404);
              res.send(response);
            }
        }
      }
    );
  } // function post makeRefund
); // post makeRefund


//
// gargallo&mozo 22/05/2018:
//
// Se define el método POST para la modificación de cuenta
// Esta funcion solo modifica el alias de la cuenta. El resto de atributos
// no son modificables
// Se devuelve una respuesta JSON
// Se devuelve resultado ok o no ok
//
app.post("/apitechu/v2/modifyAccount",
  function(req, res) {
    console.log("[modifyAccount] POST /apitechu/v2/modifyAccount");
    console.log("iban is " + req.body.iban);
    console.log("new_alias is " + req.body.alias);

    // Se crea el cliente http con la URL de base que se ha definido
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("[modifyAccount] HTTP client created")

    // Se comprueba el usuario id
    // Se crea la URL de consulta de la coleccion user
    // Se responde
    var iban = req.body.iban;
    var query = 'q={"iban":"' + iban + '"}';
    console.log(query);

    // Se lanza la consulta construida en la URL
    httpClient.get("account?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body){
        var response = {};

        if (err) {
          response = {
            "msg" : "[modifyAccount: 500] Error checking account exists",
            "reserror" : true
          }
          res.status(500);
          res.send(response);
        } else {
            if (body.length > 0) {
              response = body;

              // Cuenta existe, se modifican los campos del body
              var putBody = '{"$set":{"alias":"'+ req.body.alias + '"}}';

              httpClient.put("account?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                function(errPut, resMLabPut, bodyPut) {
                  var responsePut = {};
                  if (errPut)
                  {
                    response = {
                      "msg" : "[modifyAccount: 500] Error setting alias for account",
                      "reserror" : true
                    }
                    res.status(500);
                    res.send(response);
                  }
                  else
                  {
                    response = {
                      "msg" : "[modifyAccount] Account (alias) correctly modified",
                      "reserror" : false
                    };
                    res.send(response);
                  }
                } // function put
              );
            }
            else {
              // Cuenta no existe, se devuelve error
              response = {
                "msg" : "[modifyAccount: 404] Account not found",
                "reserror" : true
              }
              res.status(404);
              res.send(response);
            }
        }
      }
    );
  } // function post manejadora
); // post modifyAccount


//
// gargallo&mozo 25/05/2018:
//
// Se define el método POST para el login por NIF
// Se asume que solo existe un usuario por NIF
// Se devuelve una respuesta JSON
// Se devuelve resultado ok o no ok
//
app.post("/apitechu/v2/loginNIF",
  function (req,res){
    console.log ("[loginNIF] POST /apitechu/v2/loginNIF");
    console.log("nif is " + req.body.nif);
    console.log("password is " + req.body.password);

    // Se crea el cliente http con la URL de base que se ha definido
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("[loginNIF] HTTP client created")

    // Se comprueba el usuario nif
    // Se crea la URL de consulta de la coleccion user
    //var query = 'q={"nif":"' + req.body.nif + '","password":"'+ req.body.password + '"}';
    var query = 'q={"nif":"' + req.body.nif + '"}';
    console.log(query);

    // Se lanza la consulta construida en la URL
    httpClient.get ("user?" + query  + "&" + mLabAPIKey,
      function(err,resMLab,body){
        var response = {};

        if (err){
          response = {
            "msg" : "[loginNIF: 500] Error checking user by NIF" + req.body.nif,
            "reserror" : true
          };
          res.status(500);
          res.send(response);
        } else {
          if (body.length > 0)
          {
            response = body;
            var nifUser= body[0].nif;
            var idUser= body[0].id;
            var salt = bcrypt.genSaltSync(sizeSaltSync);
            var passwd= body[0].password;
            console.log(passwd);
            console.log(req.body.password);
            if (bcrypt.compareSync(req.body.password,passwd) )
            {
              // Usuario existe, se marca como logado
              var putBody = '{"$set":{"logged":true}}';

              console.log("USUARIO ENCONTRADO");
              console.log("[putBody] " + putBody);
              console.log("[put] " + "user?" + query + "&" + mLabAPIKey);

              httpClient.put ("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
              function(err2,resMLab2,bodyPut){
                if (err){
                  response = {
                    "msg" : "[loginNIF: 500] Error settin logged",
                    "reserror" : true
                  };
                  res.status(500);
                  res.send(response);
                } else {
                    response = {
                      "msg" : "[loginNIF] User logged OK",
                      "nif": nifUser,
                      "id" : idUser,
                      "reserror" : false
                    };
                    res.send(response);
                  }
              });
          }else{
            response = {
              "msg": "[loginNIF: 404] Password not corrrect",
              "reserror" : true
            };
            res.status (404);
            res.send(response);
          }
          } else {
            response = {
              "msg": "[loginNIF: 404] User not found",
              "reserror" : true
            };
            res.status (404);
            res.send(response);
          }
        }
      });
    }// function post manejadora
);// post loginNIF


//
// gargallo&mozo 22/05/2018:
//
// Se define el método POST para el traspaso
// PRE: Se debe llamar con dos cuentas del mismo usuario
// Se devuelve una respuesta JSON
// Se devuelve resultado ok o no ok
//
//
app.post("/apitechu/v2/makeInternalTransfer",
  function(req, res) {
    console.log("[makeInternalTransfer] POST /apitechu/v2/makeInternalTransfer");

    // Se crea el cliente http con la URL de base que se ha definido
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("[makeInternalTransfer] HTTP client created")

    // Se almacenan las variables del body
    var iban = req.body.iban;
    var other_iban = req.body.other_iban;
    var concept = req.body.concept;
    var currency = req.body.currency;
    var amount = req.body.amount;

    // Se comprueba la existencia de la cuenta origen
    var queryOrigin = 'q={"iban":"' + iban + '"}';
    console.log(queryOrigin);

    // Se lanza la consulta construida en la URL
    httpClient.get("account?" + queryOrigin + "&" + mLabAPIKey,
      function(err, resMLab, body){

        var response = {};
        if (err) {
          // Error en la consulta de la cuenta origen
          response = {
            "msg" : "[makeInternalTransfer: 500] Error checking origin account",
            "reserror" : true
          }
          res.status(500);
          res.send(response);
        } else {
            // Exito en la consulta de la cuenta origen
            if (body.length > 0) {
              // La cuenta origen existe, se almacena el saldo y se chequea
              // la existencia de la cuenta destino
              // En body[0].balance se dispone del saldo
              var balanceOrigin = body[0].balance;

              // Se comprueba la existencia de la cuenta destino
              var queryDest = 'q={"iban":"' + other_iban + '"}';
              console.log(queryDest);

              // Se lanza la consulta construida en la URL
              httpClient.get("account?" + queryDest + "&" + mLabAPIKey,
                function(err, resMLab, body){
                  var response = {};
                  if (err) {
                    // Error en la consulta de la cuenta destino
                    response = {
                      "msg" : "[makeInternalTransfer: 500] Error checking destination account",
                      "reserror" : true
                    }
                    res.status(500);
                    res.send(response);
                  } else {
                      if (body.length > 0){
                        // La cuenta destino existe, se almacena el saldo
                        // En body[0].balance se dispone del saldo
                        var balanceDestination = body[0].balance;

                        // Se comprueba el saldo de la cuenta origen
                        if (balanceOrigin >= amount ) {
                          // Saldo suficiente: actualizar saldos e insertar movimientos

                          // Se inserta movimiento de cargo en origen
                          var fecha = new Date();
                          var newMovementCargo = {
                            "iban": iban,
                            "other_iban" : other_iban,
                            "type": "cargo",
                            "sub_type": "Traspaso emitido" ,
                            "concept": concept,
                            "currency": currency,
                            "date": fecha,
                            "amount": amount
                          };
                          httpClient.post("movement?" + mLabAPIKey ,newMovementCargo,
                          function(err,resMLabMov,bodyPost){
                          if (err){
                            // error en la insercion de movimiento
                            response = {
                              "msg" : "[makeInternalTransfer: 500] Error inserting movement in origin account",
                              "reserror" : true
                            }
                            res.status(500);
                            res.send(response);
                          } else
                          {
                            // Movimiento cargo insertado correctamente
                            // Se inserta movimiento abono
                            var newMovementAbono = {
                              "iban": other_iban,
                              "other_iban" : iban,
                              "type": "abono",
                              "sub_type": "Traspaso recibido" ,
                              "concept": concept,
                              "currency": currency,
                              "date": fecha,
                              "amount": amount
                            };

                            httpClient.post("movement?" + mLabAPIKey ,newMovementAbono,
                            function(err,resMLabMov,bodyPost){
                            if (err){
                              // error en la insercion de movimiento
                              response = {
                                "msg" : "[makeInternalTransfer: 500] Error inserting movement in destination account",
                                "reserror" : true
                              }
                              res.status(500);
                              res.send(response);
                            } else
                            {
                              // Movimiento insertado correctamente
                              // Se actualizan saldos
                              var newBalanceOrigin = balanceOrigin - amount;
                              var putBody = '{"$set":{"balance":'+  newBalanceOrigin + '}}';
                              console.log(putBody);
                              httpClient.put("account?" + queryOrigin + "&" + mLabAPIKey, JSON.parse(putBody),
                                function(errPut, resMLabPut, bodyPut) {
                                  var responsePut = {};
                                  if (errPut)
                                  {
                                    // error en Put: no se actualiza saldo ni se inserta movimiento
                                    response = {
                                      "msg" : "[makeInternalTransfer: 500] Error updating origin balance",
                                      "reserror" : true
                                    }
                                    res.status(500);
                                    res.send(response);
                                  }
                                  else
                                  {
                                    console.log("[makeInternalTransfer] OK saldo origen");
                                    // Actualizado saldo cuenta origen
                                    // Se actualiza saldo cuenta destino
                                    var newBalanceDest = balanceDestination + amount;
                                    var putBody = '{"$set":{"balance":'+  newBalanceDest + '}}';
                                    console.log(putBody);
                                    httpClient.put("account?" + queryDest + "&" + mLabAPIKey, JSON.parse(putBody),
                                      function(errPut, resMLabPut, bodyPut) {
                                        var responsePut = {};
                                        if (errPut)
                                        {
                                          // error en Put: no se actualiza saldo ni se inserta movimiento
                                          response = {
                                            "msg" : "[makeInternalTransfer: 500] Error updating destination balance",
                                            "reserror" : true
                                          }
                                          res.status(500);
                                          res.send(response);
                                        }
                                        else
                                        {
                                          console.log("[makeInternalTransfer] OK saldo destino");
                                          response = {
                                            "msg" : "[makeInternalTransfer] Internal transfer correctly made",
                                            "reserror" : false
                                          };
                                          res.send(response);
                                        }
                    				          }
                    				        );
                                  }
                              }
                            );
                           }
                          }
                          ); // httpClient insert movimiento abono
                          }
                        }
                      ); // httpClient insert movimiento cargo
                    }
                        else {
                          // Saldo insuficiente: la transferencia no se realiza
                           response = {
                               "msg" : "[makeInternalTransfer] Internal transfer not made: not enough balance in origin account",
                               "reserror" : true
                           }
                           res.send(response);
                        }
                      }
                      else {
                        // La cuenta destino no existe, se devuelve error
                        response = {
                          "msg" : "[makeInternalTransfer: 404] Destination account not found",
                          "reserror" : true
                        }
                        res.status(404);
                        res.send(response);
                      }
                  }
                }
              );
            }
            else {
              // La cuenta origen no existe, se devuelve error
              response = {
                "msg" : "[makeInternalTransfer: 404] Origin account not found",
                "reserror" : true
              }
              res.status(404);
              res.send(response);
            }
        }
      }
    );
  } // function post makeInternalTransfer
); // post makeInternalTransfer


//
// gargallo&mozo 27/05/2018:
//
// Se define el método GET  para recuperar el listado de movimientos de una cuenta
// filtrando por importe
// Se devuelve una respuesta JSON
// Se devuelve un listado de movimientos
//
app.get("/apitechu/v2/accounts/amountFilter/:iban/:low/:high",
  function(req,res) {
    console.log("[amountFilter] GET /apitechu/v2/accounts/amountFilter/:iban/:low/:high");

    // Se crea el cliente http con la URL de base que se ha definido
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("[amountFilter] HTTP client created")

    // se almacenan los parametros de entrada
    var iban = req.params.iban;
    var lowAmount = req.params.low;
    var highAmount = req.params.high;
    console.log("[amountFilter] iban:" + iban);
    console.log("[amountFilter] low:" + lowAmount);
    console.log("[amountFilter] high:" + highAmount);

    // Se consultan los movimientos de la cuenta, filtrando por importe
    var query2 = 'q={"iban":"' + iban + '"}&s={"date":-1}';
    var query = 'q={"$and":[{"iban":"' + iban + '"},{"$and":[{"amount":{$lte:' + highAmount + '}},{"amount":{$gte:' + lowAmount + '}}]}]}&s={"date":-1}';
    console.log(query);
    console.log(query2);

    httpClient.get ("movement?" + query + "&" + mLabAPIKey,
      function(err,resMLab,body){
        var response = {};
        if (err){
          response =  {
            "msg" : "[amountFilter] Error obteniendo movimientos cuenta " + iban,
            "reserror" : true
          };
          res.status(500);
          res.send(response);
        } else
        {
          if (body.length > 0)
          {
            response = body;
            res.send(response);
          } else {
            response = {
              "msg": "[amountFilter] No hay movimientos para iban: " + iban,
              "reserror" : true
            }
            res.status (404);
            res.send(response);
          }
        }
      } // funcion manejadora get movement
    ); // get movement
  } // funcion manejadora principal
); // get amountFilter
